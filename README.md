# Magicodes.Miniprogram

#### 项目介绍
Magicodes框架之小程序前端框架

#### 软件架构
基于Taro+Dva 的多端开发解决方案

## 学习资源

[此框架需要es2015基础](http://es6.ruanyifeng.com/)

[DvaJs](https://dvajs.com/)

[完整的案例参考](https://github.com/EasyTuan/taro-msparis)

[awesome-taro](https://github.com/NervJS/awesome-taro)

## Taro 特性

#### React 语法风格

Taro 的语法规则基于 React 规范，它采用与 React 一致的组件化思想，组件生命周期与 React 保持一致，同时在书写体验上也尽量与 React 类似，支持使用 JSX 语法，让代码具有更丰富的表现力。

代码示例

```javascript
import Taro, { Component } from '@tarojs/taro'
import { View, Button } from '@tarojs/components'

export default class Index extends Component {
  constructor () {
    super(...arguments)
    this.state = {
      title: '首页',
      list: [1, 2, 3]
    }
  }

  componentWillMount () {}

  componentDidMount () {}

  componentWillUpdate (nextProps, nextState) {}

  componentDidUpdate (prevProps, prevState) {}

  shouldComponentUpdate (nextProps, nextState) {
    return true
  }

  add = (e) => {
    // dosth
  }

  render () {
    return (
      <View className='index'>
        <View className='title'>{this.state.title}</View>
        <View className='content'>
          {this.state.list.map(item => {
            return (
              <View className='item'>{item}</View>
            )
          })}
          <Button className='add' onClick={this.add}>添加</Button>
        </View>
      </View>
    )
  }
}
```



#### 安装教程

1. npm i
2. npm start (默认h5，具体命令见scripts)
